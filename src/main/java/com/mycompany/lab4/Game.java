/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');

    }

    public void play() {
        newGame();
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputNumber();
            if (table.checkWin()) {
                printTable();
                showWin();
                saveWin();
                statusCount();
                if(checkContinue()=='y'){
                    newGame();
                    continue;
                }
                break;
            }if(table.checkDraw()){
                printTable();
                showDraw();
                saveDraw();
                statusCount();
                if(checkContinue()=='y'){
                    newGame();
                    continue;
                }
                break;
            }
            table.switchPlayer();
        }
    }

    public void printWelcome() {
        System.out.println("Welcome To OX Game");
    }

    public void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }

    }

    public void printTurn() {
        System.out.println(table.getCurrentPlayer() + " Turn");
    }

    public void newGame() {
        table = new Table(player1, player2);
    }

    public void inputNumber() {
        Scanner kb = new Scanner(System.in);
        while(true){
        System.out.print("Please intput Number: ");
        char num = kb.next().charAt(0);
        if(table.setInput(num)){
            return;
            }
        }

    }

    public void showWin() {
        System.out.println(table.getCurrentPlayer() + " Win!!!");
    }

    public void showDraw() {
        System.out.println("Draw!!!");
    }
    
    public char checkContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("do you want to continue (y or n): ");
        char YesNO = kb.next().charAt(0);
        while(YesNO != 'y' && YesNO != 'n'){
            System.out.println("Enter y or n only");
            System.out.print("do you want to continue (y or n): ");
            YesNO = kb.next().charAt(0);
        }
        return  YesNO;
    }

    public void saveDraw() {
        player1.drawCount();
        player2.drawCount();
    }

    public void saveWin() {
        if(table.getCurrentPlayer()==player1.getSymbol()){
            player1.winCount();
            player2.loseCount();
        }else{
            player1.loseCount();
            player2.winCount();
        }
    }
    public void statusCount(){
        System.out.println("----------------------------------");
        System.out.println(player1.getSymbol()+" Win "+player1.getWinCount()+" round");
        System.out.println(player1.getSymbol()+" Lose "+player1.getLoseCount()+" round");
        System.out.println(player1.getSymbol()+" Draw "+player1.getDrawCount()+" round");
        System.out.println("----------------------------------");
        System.out.println(player2.getSymbol()+" Win "+player2.getWinCount()+" round");
        System.out.println(player2.getSymbol()+" Lose "+player2.getLoseCount()+" round");
        System.out.println(player2.getSymbol()+" Draw "+player2.getDrawCount()+" round");
        System.out.println("----------------------------------");
    }
}
